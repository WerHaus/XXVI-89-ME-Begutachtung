
# Langtitel
Bundesgesetz über die Zustellung behördlicher Dokumente (Zustellgesetz – ZustG)
StF: BGBl. Nr. 200/1982 (NR: GP XV RV 162 AB 1050 S. 110. BR: S. 421.)
# Änderung

BGBl. Nr. 357/1990 (NR: GP XVII RV 1089 AB 1350 S. 145. BR: AB 3889 S. 531.)

BGBl. I Nr. 158/1998 (NR: GP XX AB 1167 S. 119. BR: AB 5676 S. 642.)

BGBl. I Nr. 137/2001 (NR: GP XXI RV 723 AB 813 S. 80. BR: AB 6474 S. 681.)

BGBl. I Nr. 65/2002 (NR: GP XXI RV 772 AB 885 S. 83. BR: 6488 AB 6496 S. 682.)

BGBl. I Nr. 10/2004 (NR: GP XXII RV 252 AB 382 S. 46. BR: 6959 AB 6961 S. 705.)

BGBl. I Nr. 5/2008 (NR: GP XXIII RV 294 AB 365 S. 41. BR: 7800 AB 7835 S. 751.)

BGBl. I Nr. 111/2010 (NR: GP XXIV RV 981 AB 1026 S. 90. BR: 8437 AB 8439 S. 792.)

[CELEX-Nr.: 32010L0012]

BGBl. I Nr. 33/2013 (NR: GP XXIV RV 2009 AB 2112 S. 187. BR: 8882 AB 8891 S. 817.)

BGBl. I Nr. 40/2017 (NR: GP XXV RV 1457 AB 1569 S. 171. BR: 9747 AB 9752 S. 866.)

[CELEX-Nr.: 32009L0031]

BGBl. I Nr. 33/2018 (K über Idat)
Sonstige Textteile

Der Nationalrat hat beschlossen:
Präambel/Promulgationsklausel



# Inhaltsverzeichnis

(Anm.: wurde nicht im BGBl. kundgemacht)

1. Abschnitt
	Allgemeine Bestimmungen

	§ 1.  Anwendungsbereich
	§ 2.  Begriffsbestimmungen
	§ 3.  Durchführung der Zustellung
	§ 4.  Stellung des Zustellers
	§ 5.  Zustellverfügung
	§ 6.  Mehrmalige Zustellung
	§ 7.  Heilung von Zustellmängeln
	§ 8.  Änderung der Abgabestelle
	§ 9.  Zustellungsbevollmächtigter
	§ 10.  Zustellung durch Übersendung
	§ 11.  Besondere Fälle der Zustellung
	§ 12.  Zustellung ausländischer Dokumente im Inland

2. Abschnitt
	Physische Zustellung

	§§ 13 – 15.  Zustellung an den Empfänger
	§ 16.  Ersatzzustellung
	§ 17.  Hinterlegung
	§ 18.  Nachsendung
	§ 19.  Rücksendung, Weitersendung und Vernichtung
	§ 20.  Verweigerung der Annahme
	§ 21.  Zustellung zu eigenen Handen
	§ 22.  Zustellnachweis
	§ 23.  Hinterlegung ohne Zustellversuch
	§ 24.  Unmittelbare Ausfolgung
	§ 24a.  Zustellung am Ort des Antreffens
	§ 25.  Zustellung durch öffentliche Bekanntmachung
	§ 26.  Zustellung ohne Zustellnachweis
	§ 27.  Ausstattung der Dokumente; Zustellformulare; Zustellnachweise

3. Abschnitt
	Elektronische Zustellung

	§ 28.  Anwendungsbereich
	§ 29.  Leistungen der Zustelldienste
	§ 30.  Zulassung als Zustelldienst
	§ 31.  Aufsicht
	§ 32.  Bestimmung des Ermittlungs- und Zustelldienstes
	§ 33.  An- und Abmeldung
	§ 34.  Ermittlung des Zustelldienstes und Übermittlung des zuzustellenden Dokuments an diesen
	§ 35.  Zustellung mit Zustellnachweis durch einen Zustelldienst
	§ 36.  Zustellung ohne Zustellnachweis durch einen Zustelldienst
	§ 37.  Zustellung an einer elektronischen Zustelladresse oder über das elektronische Kommunikationssystem der Behörde
	§ 37a.  Unmittelbare elektronische Ausfolgung
	§ 37b.  Anzeigemodul

4. Abschnitt
	Schlußbestimmungen

	§ 38.  Verweisungen
	§ 39.  Vollziehung
	§ 40.  Inkrafttreten
	§ 41.  Sprachliche Gleichbehandlung


# Text

## 1. Abschnitt: Allgemeine Bestimmungen

### Anwendungsbereich

<a name="p-1"></a>
§ 1. Dieses Bundesgesetz regelt die Zustellung der von Gerichten und Verwaltungsbehörden in Vollziehung der Gesetze zu übermittelnden Dokumente sowie die durch sie vorzunehmende Zustellung von Dokumenten ausländischer Behörden.

### Begriffsbestimmungen

<a name="p-2"></a>
§ 2. Im Sinne dieses Bundesgesetzes bedeuten die Begriffe:


	1.  „Empfänger“: die von der Behörde in der Zustellverfügung ([§ 5](#p-5)) namentlich als solcher bezeichnete Person;
	2.  „Dokument“: eine Aufzeichnung, unabhängig von ihrer technischen Form, insbesondere eine behördliche schriftliche Erledigung;
	3.  „Zustelladresse“: eine Abgabestelle (Z 4) oder elektronische Zustelladresse (Z 5);
	4.  „Abgabestelle“: die Wohnung oder sonstige Unterkunft, die Betriebsstätte, der Sitz, der Geschäftsraum, die Kanzlei oder auch der Arbeitsplatz des Empfängers, im Falle einer Zustellung anlässlich einer Amtshandlung auch deren Ort, oder ein vom Empfänger der Behörde für die Zustellung in einem laufenden Verfahren angegebener Ort;
	5.  „elektronische Zustelladresse“: eine vom Empfänger der Behörde für die Zustellung in einem anhängigen oder gleichzeitig anhängig gemachten Verfahren angegebene elektronische Adresse;
	6.  „Post“: die Österreichische Post AG (§ 3 Z 1 des Postmarktgesetzes – PMG, BGBl. I Nr. 123/2009);
	7.  „Zustelldienst“: ein Universaldienstbetreiber (§ 3 Z 4 PMG) sowie ein Zustelldienst im Anwendungsbereich des 3. Abschnitts;
	8.  „Ermittlungs- und Zustelldienst“: der Zustelldienst, der die Leistungen gemäß [§ 29 Abs. 2](#p-29) zu erbringen hat;
	9.  „Kunde“: Person, gegenüber der sich ein Zustelldienst, der die Leistungen gemäß [§ 29 Abs. 1](#-29) zu erbringen hat, zur Zustellung behördlicher Dokumente verpflichtet hat.

<a name="p-3"></a>
### Durchführung der Zustellung

§ 3. Soweit die für das Verfahren geltenden Vorschriften nicht eine andere Form der Zustellung vorsehen, hat die Zustellung durch einen Zustelldienst, durch Bedienstete der Behörde oder, wenn dies im Interesse der Zweckmäßigkeit, Einfachheit und Raschheit gelegen ist, durch Organe der Gemeinden zu erfolgen.

<a name="p-4"></a>
### Stellung des Zustellers

§ 4. Wer mit der Zustellung betraut ist (Zusteller), handelt hinsichtlich der Wahrung der Gesetzmäßigkeit der Zustellung als Organ der Behörde, deren Dokument zugestellt werden soll.

<a name="p-5"></a>
### Zustellverfügung

§ 5. Die Zustellung ist von der Behörde zu verfügen, deren Dokument zugestellt werden soll. Die Zustellverfügung hat den Empfänger möglichst eindeutig zu bezeichnen und die für die Zustellung erforderlichen sonstigen Angaben zu enthalten.

<a name="p-6"></a>
### Mehrmalige Zustellung

§ 6. Ist ein Dokument zugestellt, so löst die neuerliche Zustellung des gleichen Dokuments keine Rechtswirkungen aus.

<a name="p-7"></a>
### Heilung von Zustellmängeln

§ 7. Unterlaufen im Verfahren der Zustellung Mängel, so gilt die Zustellung als in dem Zeitpunkt dennoch bewirkt, in dem das Dokument dem Empfänger tatsächlich zugekommen ist.

<a name="p-8"></a>
### Änderung der Abgabestelle

§ 8. (1) Eine Partei, die während eines Verfahrens, von dem sie Kenntnis hat, ihre bisherige Abgabestelle ändert, hat dies der Behörde unverzüglich mitzuteilen.

(2) Wird diese Mitteilung unterlassen, so ist, soweit die Verfahrensvorschriften nicht anderes vorsehen, die Zustellung durch Hinterlegung ohne vorausgehenden Zustellversuch vorzunehmen, falls eine Abgabestelle nicht ohne Schwierigkeiten festgestellt werden kann.

<a name="p-9"></a>
### Zustellungsbevollmächtigter

§ 9. (1) Soweit in den Verfahrensvorschriften nicht anderes bestimmt ist, können die Parteien und Beteiligten andere natürliche oder juristische Personen oder eingetragene Personengesellschaften gegenüber der Behörde zur Empfangnahme von Dokumenten bevollmächtigen (Zustellungsvollmacht).

(2) Einer natürlichen Person, die keinen Hauptwohnsitz im Inland hat, kann eine Zustellungsvollmacht nicht wirksam erteilt werden. Gleiches gilt für eine juristische Person oder eingetragene Personengesellschaft, wenn diese keinen zur Empfangnahme von Dokumenten befugten Vertreter mit Hauptwohnsitz im Inland hat. Das Erfordernis des Hauptwohnsitzes im Inland gilt nicht für Staatsangehörige von EWR-Vertragsstaaten, falls Zustellungen durch Staatsverträge mit dem Vertragsstaat des Wohnsitzes des Zustellungsbevollmächtigten oder auf andere Weise sichergestellt sind.

(3) Ist ein Zustellungsbevollmächtigter bestellt, so hat die Behörde, soweit gesetzlich nicht anderes bestimmt ist, diesen als Empfänger zu bezeichnen. Geschieht dies nicht, so gilt die Zustellung als in dem Zeitpunkt bewirkt, in dem das Dokument dem Zustellungsbevollmächtigten tatsächlich zugekommen ist.

(4) Haben mehrere Parteien oder Beteiligte einen gemeinsamen Zustellungsbevollmächtigten, so gilt mit der Zustellung einer einzigen Ausfertigung des Dokumentes an ihn die Zustellung an alle Parteien oder Beteiligte als bewirkt. Hat eine Partei oder hat ein Beteiligter mehrere Zustellungsbevollmächtigte, so gilt die Zustellung als bewirkt, sobald sie an einen von ihnen vorgenommen worden ist.

(5) Wird ein Anbringen von mehreren Parteien oder Beteiligten gemeinsam eingebracht und kein Zustellungsbevollmächtigter namhaft gemacht, so gilt die an erster Stelle genannte Person als gemeinsamer Zustellungsbevollmächtigter.

(6) [§ 8](#p-8) ist auf den Zustellungsbevollmächtigten sinngemäß anzuwenden.

<a name="p-10"></a>
### Zustellung durch Übersendung

§ 10. (1) Parteien und Beteiligten, die über keine inländische Abgabestelle verfügen, kann von der Behörde aufgetragen werden, innerhalb einer Frist von mindestens zwei Wochen für bestimmte oder für alle bei dieser Behörde anhängigen oder anhängig zu machenden Verfahren einen Zustellungsbevollmächtigten namhaft zu machen. Kommt die Partei bzw. der Beteiligte diesem Auftrag nicht fristgerecht nach, kann die Zustellung ohne Zustellnachweis durch Übersendung der Dokumente an eine der Behörde bekannte Zustelladresse erfolgen. Ein übersandtes Dokument gilt zwei Wochen nach Übergabe an den Zustelldienst als zugestellt. Auf diese Rechtsfolge ist im Auftrag hinzuweisen.

(2) Eine Zustellung gemäß Abs. 1 ist nicht mehr zulässig, sobald die Partei bzw. der Beteiligte


	1.  einen Zustellungsbevollmächtigten namhaft gemacht hat oder
	2.  über eine inländische Abgabestelle verfügt und diese der Behörde bekannt gegeben hat.

<a name="p-11"></a>
### Besondere Fälle der Zustellung

§ 11. (1) Zustellungen im Ausland sind nach den bestehenden internationalen Vereinbarungen oder allenfalls auf dem Weg, den die Gesetze oder sonstigen Rechtsvorschriften des Staates, in dem zugestellt werden soll, oder die internationale Übung zulassen, erforderlichenfalls unter Mitwirkung der österreichischen Vertretungsbehörden, vorzunehmen.

(2) Zur Vornahme von Zustellungen an Ausländer oder internationale Organisationen, denen völkerrechtliche Privilegien und Immunitäten zustehen, ist unabhängig von ihrem Aufenthaltsort oder Sitz die Vermittlung des Bundesministeriums für Europa, Integration und Äußeres in Anspruch zu nehmen.

(3) Zustellungen an Personen, die nach den Vorschriften des Bundesverfassungsgesetzes über Kooperation und Solidarität bei der Entsendung von Einheiten und Einzelpersonen in das Ausland (KSE-BVG), BGBl. I Nr. 38/1997, in das Ausland entsendet wurden, sind im Wege des zuständigen Bundesministers, sofern aber diese Personen anlässlich ihrer Entsendung zu einer Einheit oder zu mehreren Einheiten zusammengefasst wurden, im Wege des Vorgesetzten der Einheit vorzunehmen.

<a name="p-12"></a>
### Zustellung ausländischer Dokumente im Inland

§ 12. (1) Zustellungen von Dokumenten ausländischer Behörden im Inland sind nach den bestehenden internationalen Vereinbarungen, mangels solcher nach diesem Bundesgesetz vorzunehmen. Einem Ersuchen um Einhaltung einer bestimmten davon abweichenden Vorgangsweise kann jedoch entsprochen werden, wenn eine solche Zustellung mit den Grundwertungen der österreichischen Rechtsordnung vereinbar ist.

(2) Die Zustellung eines ausländischen, fremdsprachigen Dokuments, dem keine, im gerichtlichen Verfahren keine beglaubigte, deutschsprachige Übersetzung angeschlossen ist, ist nur zulässig, wenn der Empfänger zu dessen Annahme bereit ist; dies ist anzunehmen, wenn er nicht binnen drei Tagen gegenüber der Behörde, die das Dokument zugestellt hat, erklärt, daß er zur Annahme nicht bereit ist; diese Frist beginnt mit der Zustellung zu laufen und kann nicht verlängert werden.

(3) Ist die Erklärung gemäß Abs. 2 verspätet oder unzulässig, so ist sie zurückzuweisen; sonst hat die Behörde zu beurkunden, daß die Zustellung des fremdsprachigen Dokuments mangels Annahmebereitschaft des Empfängers als nicht bewirkt anzusehen ist.

(4) Für die Zustellung von Dokumenten ausländischer Behörden in Verwaltungssachen gelten, falls in Staatsverträgen nicht anderes bestimmt ist, außerdem die folgenden Bestimmungen:


	1.  Dokumente werden nur zugestellt, wenn gewährleistet ist, dass auch der ersuchende Staat einem gleichartigen österreichischen Ersuchen entsprechen würde. Das Vorliegen von Gegenseitigkeit kann durch Staatsverträge, die nicht unter Art. 50 B-VG fallen, festgestellt werden.
	2.  Im Übrigen sind das Europäische Übereinkommen über die Zustellung von Schriftstücken in Verwaltungssachen im Ausland, BGBl. Nr. 67/1983, und die von der Republik Österreich gemäß diesem Abkommen abgegebenen Erklärungen sinngemäß anzuwenden.

<a name="p-13"></a>
## 2. Abschnitt: Physische Zustellung

### Zustellung an den Empfänger

§ 13. (1) Das Dokument ist dem Empfänger an der Abgabestelle zuzustellen. Ist aber auf Grund einer Anordnung einer Verwaltungsbehörde oder eines Gerichtes an eine andere Person als den Empfänger zuzustellen, so tritt diese an die Stelle des Empfängers.

(2) Bei Zustellungen durch Organe eines Zustelldienstes oder der Gemeinde darf auch an eine gegenüber dem Zustelldienst oder der Gemeinde zur Empfangnahme solcher Dokumente bevollmächtigte Person zugestellt werden, soweit dies nicht durch einen Vermerk auf dem Dokument ausgeschlossen ist.

(3) Ist der Empfänger keine natürliche Person, so ist das Dokument einem zur Empfangnahme befugten Vertreter zuzustellen.

(4) Ist der Empfänger eine zur berufsmäßigen Parteienvertretung befugte Person, so ist das Dokument in deren Kanzlei zuzustellen und darf an jeden dort anwesenden Angestellten des Parteienvertreters zugestellt werden; durch Organe eines Zustelldienstes darf an bestimmte Angestellte nicht oder nur an bestimmte Angestellte zugestellt werden, wenn der Parteienvertreter dies schriftlich beim Zustelldienst verlangt hat. Die Behörde hat Angestellte des Parteienvertreters wegen ihres Interesses an der Sache oder auf Grund einer zuvor der Behörde schriftlich abgegebenen Erklärung des Parteienvertreters durch einen Vermerk auf dem Dokument und dem Zustellnachweis von der Zustellung auszuschließen; an sie darf nicht zugestellt werden.

(Anm.: Abs. 5 und 6 aufgehoben durch BGBl. I Nr. 10/2004)

<a name="p-14"></a>
§ 14. Untersteht der Empfänger einer Anstaltsordnung und dürfen ihm auf Grund gesetzlicher Bestimmungen Dokumente nur durch den Leiter der Anstalt oder durch eine von diesem bestimmte Person oder durch den Untersuchungsrichter ausgehändigt werden, so ist das Dokument dem Leiter der Anstalt oder der von ihm bestimmten Person vom Zusteller zur Vornahme der Zustellung zu übergeben.

<a name="p-15"></a>
§ 15. (1) Zustellungen an Soldaten, die Präsenz- oder Ausbildungsdienst leisten, sind durch das unmittelbar vorgesetzte Kommando vorzunehmen.

(2) Bei sonstigen Zustellungen in Kasernen oder auf anderen militärisch genutzten Liegenschaften ist das für deren Verwaltung zuständige Kommando vorher davon in Kenntnis zu setzen. Auf Verlangen des Kommandos ist ein von ihm zu bestimmender Soldat oder Bediensteter der Heeresverwaltung dem Zusteller beizugeben.
<a name="p-16"></a>

### Ersatzzustellung

§ 16. (1) Kann das Dokument nicht dem Empfänger zugestellt werden und ist an der Abgabestelle ein Ersatzempfänger anwesend, so darf an diesen zugestellt werden (Ersatzzustellung), sofern der Zusteller Grund zur Annahme hat, daß sich der Empfänger oder ein Vertreter im Sinne des [§ 13 Abs. 3](#p-13) regelmäßig an der Abgabestelle aufhält.

(2) Ersatzempfänger kann jede erwachsene Person sein, die an derselben Abgabestelle wie der Empfänger wohnt oder Arbeitnehmer oder Arbeitgeber des Empfängers ist und die – außer wenn sie mit dem Empfänger im gemeinsamen Haushalt lebt – zur Annahme bereit ist.

(3) Durch Organe eines Zustelldienstes darf an bestimmte Ersatzempfänger nicht oder nur an bestimmte Ersatzempfänger zugestellt werden, wenn der Empfänger dies schriftlich beim Zustelldienst verlangt hat.

(4) Die Behörde hat Personen wegen ihres Interesses an der Sache oder auf Grund einer schriftlichen Erklärung des Empfängers durch einen Vermerk auf dem Dokument und dem Zustellnachweis von der Ersatzzustellung auszuschließen; an sie darf nicht zugestellt werden.

(5) Eine Ersatzzustellung gilt als nicht bewirkt, wenn sich ergibt, daß der Empfänger oder dessen Vertreter im Sinne des [§ 13 Abs. 3](#p-13) wegen Abwesenheit von der Abgabestelle nicht rechtzeitig vom Zustellvorgang Kenntnis erlangen konnte, doch wird die Zustellung mit dem der Rückkehr an die Abgabestelle folgenden Tag wirksam.
<a name="p-17"></a>

### Hinterlegung

§ 17. (1) Kann das Dokument an der Abgabestelle nicht zugestellt werden und hat der Zusteller Grund zur Annahme, daß sich der Empfänger oder ein Vertreter im Sinne des [§ 13 Abs. 3](#p-13) regelmäßig an der Abgabestelle aufhält, so ist das Dokument im Falle der Zustellung durch den Zustelldienst bei seiner zuständigen Geschäftsstelle, in allen anderen Fällen aber beim zuständigen Gemeindeamt oder bei der Behörde, wenn sie sich in derselben Gemeinde befindet, zu hinterlegen.

(2) Von der Hinterlegung ist der Empfänger schriftlich zu verständigen. Die Verständigung ist in die für die Abgabestelle bestimmte Abgabeeinrichtung (Briefkasten, Hausbrieffach oder Briefeinwurf) einzulegen, an der Abgabestelle zurückzulassen oder, wenn dies nicht möglich ist, an der Eingangstüre (Wohnungs-, Haus-, Gartentüre) anzubringen. Sie hat den Ort der Hinterlegung zu bezeichnen, den Beginn und die Dauer der Abholfrist anzugeben sowie auf die Wirkung der Hinterlegung hinzuweisen.

(3) Das hinterlegte Dokument ist mindestens zwei Wochen zur Abholung bereitzuhalten. Der Lauf dieser Frist beginnt mit dem Tag, an dem das Dokument erstmals zur Abholung bereitgehalten wird. Hinterlegte Dokumente gelten mit dem ersten Tag dieser Frist als zugestellt. Sie gelten nicht als zugestellt, wenn sich ergibt, daß der Empfänger oder dessen Vertreter im Sinne des [§ 13 Abs. 3](#p-13) wegen Abwesenheit von der Abgabestelle nicht rechtzeitig vom Zustellvorgang Kenntnis erlangen konnte, doch wird die Zustellung an dem der Rückkehr an die Abgabestelle folgenden Tag innerhalb der Abholfrist wirksam, an dem das hinterlegte Dokument behoben werden könnte.

(4) Die im Wege der Hinterlegung vorgenommene Zustellung ist auch dann gültig, wenn die im Abs. 2 genannte Verständigung beschädigt oder entfernt wurde.
<a name="p-18"></a>

### Nachsendung

§ 18. (1) Hält sich der Empfänger nicht regelmäßig ([§ 17 Abs. 1](#p-17)) an der Abgabestelle auf, so ist das Dokument an eine andere inländische Abgabestelle nachzusenden, wenn es


	1.  durch Organe eines Zustelldienstes zugestellt werden soll und nach den für die Beförderung von Postsendungen geltenden Vorschriften die Nachsendung vorgesehen ist; in diesem Fall ist die neue Anschrift des Empfängers auf dem Zustellnachweis (Zustellschein, Rückschein) zu vermerken;
	2.  durch Organe der Behörde oder einer Gemeinde zugestellt werden soll, die neue Abgabestelle ohne Schwierigkeit festgestellt werden kann und im örtlichen Wirkungsbereich der Behörde oder der Gemeinde liegt.

(2) Dokumente, deren Nachsendung durch einen auf ihnen angebrachten Vermerk ausgeschlossen ist, sind nicht nachzusenden.
<a name="p-19"></a>

### Rücksendung, Weitersendung und Vernichtung

§ 19. (1) Dokumente, die weder zugestellt werden können, noch nachzusenden sind oder die zwar durch Hinterlegung zugestellt, aber nicht abgeholt worden sind, sind entweder an den Absender zurückzusenden, an eine vom Absender zu diesem Zweck bekanntgegebene Stelle zu senden oder auf Anordnung des Absenders nachweislich zu vernichten.

(2) Auf dem Zustellnachweis (Zustellschein, Rückschein) ist der Grund der Rücksendung, Weitersendung oder Vernichtung zu vermerken.
<a name="p-20"></a>

### Verweigerung der Annahme

§ 20. (1) Verweigert der Empfänger oder ein im gemeinsamen Haushalt mit dem Empfänger lebender Ersatzempfänger die Annahme ohne Vorliegen eines gesetzlichen Grundes, so ist das Dokument an der Abgabestelle zurückzulassen oder, wenn dies nicht möglich ist, nach [§ 17](#p-17) ohne die dort vorgesehene schriftliche Verständigung zu hinterlegen.

(2) Zurückgelassene Dokumente gelten damit als zugestellt.

(3) Wird dem Zusteller der Zugang zur Abgabestelle verwehrt, verleugnet der Empfänger seine Anwesenheit, oder läßt er sich verleugnen, so gilt dies als Verweigerung der Annahme.
<a name="p-21"></a>

### Zustellung zu eigenen Handen

§ 21. Dem Empfänger zu eigenen Handen zuzustellende Dokumente dürfen nicht an einen Ersatzempfänger zugestellt werden.
<a name="p-22"></a>

### Zustellnachweis

§ 22. (1) Die Zustellung ist vom Zusteller auf dem Zustellnachweis (Zustellschein, Rückschein) zu beurkunden.

(2) Der Übernehmer des Dokuments hat die Übernahme auf dem Zustellnachweis durch seine Unterschrift unter Beifügung des Datums und, wenn er nicht der Empfänger ist, seines Naheverhältnisses zu diesem zu bestätigen. Verweigert er die Bestätigung, so hat der Zusteller die Tatsache der Verweigerung, das Datum und gegebenenfalls das Naheverhältnis des Übernehmers zum Empfänger auf dem Zustellnachweis zu vermerken. Der Zustellnachweis ist dem Absender unverzüglich zu übersenden.

(3) An die Stelle der Übersendung des Zustellnachweises kann die elektronische Übermittlung einer Kopie des Zustellnachweises oder der sich daraus ergebenden Daten treten, wenn die Behörde dies nicht durch einen entsprechenden Vermerk auf dem Zustellnachweis ausgeschlossen hat. Das Original des Zustellnachweises ist mindestens fünf Jahre nach Übermittlung aufzubewahren und der Behörde auf deren Verlangen unverzüglich zu übersenden.

(4) Liegen die technischen Voraussetzungen dafür vor, so kann die Beurkundung der Zustellung auch elektronisch erfolgen. In diesem Fall hat der Übernehmer auf einer technischen Vorrichtung zu unterschreiben; an die Stelle der Unterschriftsleistung kann auch die Identifikation und Authentifizierung mit der Bürgerkarte (§ 2 Z 10 des E-Government-Gesetzes – E-GovG, BGBl. I Nr. 10/2004) treten. Die die Beurkundung der Zustellung betreffenden Daten sind dem Absender unverzüglich zu übermitteln.
<a name="p-23"></a>

### Hinterlegung ohne Zustellversuch

§ 23. (1) Hat die Behörde auf Grund einer gesetzlichen Vorschrift angeordnet, daß ein Dokument ohne vorhergehenden Zustellversuch zu hinterlegen ist, so ist dieses sofort bei der zuständigen Geschäftsstelle des Zustelldienstes, beim Gemeindeamt oder bei der Behörde selbst zur Abholung bereitzuhalten.

(2) Die Hinterlegung ist von der zuständigen Geschäftsstelle des Zustelldienstes oder vom Gemeindeamt auf dem Zustellnachweis, von der Behörde auch auf andere Weise zu beurkunden.

(3) Soweit dies zweckmäßig ist, ist der Empfänger durch eine an die angegebene inländische Abgabestelle zuzustellende schriftliche Verständigung oder durch mündliche Mitteilung an Personen, von denen der Zusteller annehmen kann, daß sie mit dem Empfänger in Verbindung treten können, von der Hinterlegung zu unterrichten.

(4) Das so hinterlegte Dokument gilt mit dem ersten Tag der Hinterlegung als zugestellt.
<a name="p-24"></a>

### Unmittelbare Ausfolgung

§ 24. Dem Empfänger können


	1.  versandbereite Dokumente unmittelbar bei der Behörde,
	2.  Dokumente, die die Behörde an eine andere Dienststelle übermittelt hat, unmittelbar bei dieser 
ausgefolgt werden. Die Ausfolgung ist von der Behörde bzw. von der Dienststelle zu beurkunden; [§ 22 Abs. 2 bis 4](#p-22) ist sinngemäß anzuwenden.
<a name="p-24a"></a>

### Zustellung am Ort des Antreffens

§ 24a. Dem Empfänger kann an jedem Ort zugestellt werden, an dem er angetroffen wird, wenn er


	1.  zur Annahme bereit ist oder
	2.  über keine inländische Abgabestelle verfügt.
<a name="p-25"></a>

### Zustellung durch öffentliche Bekanntmachung

§ 25. (1) Zustellungen an Personen, deren Abgabestelle unbekannt ist, oder an eine Mehrheit von Personen, die der Behörde nicht bekannt sind, können, wenn es sich nicht um ein Strafverfahren handelt, kein Zustellungsbevollmächtigter bestellt ist und nicht gemäß [§ 8](#p-8) vorzugehen ist, durch Kundmachung an der Amtstafel, daß ein zuzustellendes Dokument bei der Behörde liegt, vorgenommen werden. Findet sich der Empfänger zur Empfangnahme des Dokuments ([§ 24](#p-24)) nicht ein, so gilt, wenn gesetzlich nicht anderes bestimmt ist, die Zustellung als bewirkt, wenn seit der Kundmachung an der Amtstafel der Behörde zwei Wochen verstrichen sind.

(2) Die Behörde kann die öffentliche Bekanntmachung in anderer geeigneter Weise ergänzen.
<a name="p-26"></a>

### Zustellung ohne Zustellnachweis

§ 26. (1) Wurde die Zustellung ohne Zustellnachweis angeordnet, wird das Dokument zugestellt, indem es in die für die Abgabestelle bestimmte Abgabeeinrichtung ([§ 17 Abs. 2](#p-17)) eingelegt oder an der Abgabestelle zurückgelassen wird.

(2) Die Zustellung gilt als am dritten Werktag nach der Übergabe an das Zustellorgan bewirkt. Im Zweifel hat die Behörde die Tatsache und den Zeitpunkt der Zustellung von Amts wegen festzustellen. Die Zustellung wird nicht bewirkt, wenn sich ergibt, dass der Empfänger wegen Abwesenheit von der Abgabestelle nicht rechtzeitig vom Zustellvorgang Kenntnis erlangen konnte, doch wird die Zustellung mit dem der Rückkehr an die Abgabestelle folgenden Tag wirksam.
<a name="p-27"></a>

### Ausstattung der Dokumente; Zustellformulare; Zustellnachweise

§ 27. Soweit dies erforderlich ist, hat die Bundesregierung durch Verordnung nähere Bestimmungen über


	1.  die Ausstattung der zuzustellenden Dokumente,
	2.  die bei der Zustellung verwendbaren Formulare und
	3.  die für die elektronische Übermittlung gemäß [§ 22 Abs. 3](#p-22) sowie für die Speicherung und Übermittlung der die Beurkundung der Zustellung betreffenden Daten erforderlichen technischen Voraussetzungen

zu erlassen.
<a name="p-28"></a>

## 3. Abschnitt: Elektronische Zustellung

### Anwendungsbereich

§ 28. (1) Soweit die für das Verfahren geltenden Vorschriften nicht anderes bestimmen, ist eine elektronische Zustellung nach den Bestimmungen dieses Abschnitts vorzunehmen.

(2) Die elektronische Zustellung der ordentlichen Gerichte richtet sich nach den §§ 89a ff des Gerichtsorganisationsgesetzes – GOG, RGBl. Nr. 217/1896.
<a name="p-29"></a>

### Leistungen der Zustelldienste

§ 29. (1) Jeder Zustelldienst hat nach den näheren Bestimmungen dieses Bundesgesetzes die Zustellung behördlicher Dokumente an seine Kunden vorzunehmen (Zustellleistung). Die Zustellleistung umfasst folgende, nach dem jeweiligen Stand der Technik zu erbringende Leistungen:


	1.  die unverzügliche Weiterleitung
		a) der Daten gemäß [§ 33 Abs. 1](#p-33),
		b) einer vom Kunden bekanntgegebenen Änderung dieser Daten ([§ 33 Abs. 2 erster Satz](#p-33)) sowie
		c) von Mitteilungen gemäß [§ 33 Abs. 2 zweiter Satz](#p-33)
		an den Ermittlungs- und Zustelldienst;
	2.  die Schaffung der technischen Voraussetzungen für die Entgegennahme der zuzustellenden Dokumente ([§ 34 Abs. 1](#p-34));
	3.  das Betreiben einer technischen Einrichtung für die sichere elektronische Bereithaltung der zuzustellenden Dokumente;
	4.  die Verständigung des Empfängers, dass auf der technischen Einrichtung ein Dokument für ihn zur Abholung bereitliegt ([§ 35 Abs. 1 und 2](#p-35));
	5.  die gegebenenfalls verschlüsselte ([§ 33 Abs. 1 Z 7](#p-33)) Speicherung der zuzustellenden Dokumente;
	6.  die Bereitstellung eines Verfahrens zur identifizierten und authentifizierten Abholung der bereitgehaltenen Dokumente;
	7.  die Protokollierung von Daten im Sinn des [§ 35 Abs. 3 vierter Satz](#p-35) und die Übermittlung dieser Daten an den Absender;
	8.  die unverzügliche Verständigung des Absenders, wenn ein Dokument nicht abgeholt wird;
	9.  die Beratung des Empfängers, wenn bei der Abholung von Dokumenten technische Probleme auftreten;
	10.  die Erstellung von Ausdrucken oder Kopien des zuzustellenden Dokuments auf Papier oder Kopien dieses Dokuments auf Datenträgern sowie die Übermittlung dieser Ausdrucke und Datenträger an den Empfänger auf dessen Verlangen;
	11.  sofern der Zustelldienst diese Leistung anbietet, die Weiterleitung eines zuzustellenden Dokuments zur elektronischen Übermittlung nach den §§ 89a ff GOG auf Verlangen des Empfängers sowie die Mitteilung an den Absender, wann das zuzustellende Dokument in den elektronischen Verfügungsbereich des Empfängers (§ 89d GOG) gelangt ist;
	12.  die Weiterleitung der das Dokument beschreibenden Daten sowie die elektronische Information für die technische Möglichkeit der elektronischen identifizierten und authentifizierten Abholung des Dokuments an das Anzeigemodul ([§ 37b](#p-37b)).

Die Behörde hat für die Erbringung der Leistungen gemäß Z 1 bis 9 ein Entgelt zu entrichten, dessen Höhe dem Entgelt entspricht, das dem Zuschlagsempfänger gemäß [§ 32 Abs. 1](#p-32) für die Erbringung dieser Leistungen zusteht. Das Entgelt für die Erbringung der Leistung gemäß Z 10 ist vom Empfänger zu entrichten.

(2) Einer der Zustelldienste hat außerdem folgende Leistungen zu erbringen:


	1.  die Speicherung der gemäß Abs. 1 Z 1 weitergeleiteten Daten,
	2.  die Leistungen gemäß [§ 34 Abs. 1 erster und zweiter Satz](#p-34) (Ermittlungsleistung) und
	3.  die Weiterleitung des von den Behörden für eine Zustellung entrichteten Entgelts an jene Zustelldienste, die die Zustellleistung erbracht haben, sowie die Verrechnung der weitergegebenen Entgelte mit den Behörden (Verrechnungsleistung).

Die Behörde hat für die Erbringung der Verrechnungsleistung ein Entgelt zu entrichten.

(3) Zustelldienste können weitere Leistungen, wie insbesondere die nachweisliche Zusendung von Dokumenten im Auftrag von Privaten, entgeltlich anbieten. Für die nachweisliche Zusendung von Dokumenten im Auftrag von Privaten hat der Ermittlungs- und Zustelldienst die Ermittlungsleistung (Abs. 2 Z 2) zu denselben Bedingungen wie bei der Zustellung behördlicher Dokumente zu erbringen.

(4) Zustelldienste sind hinsichtlich der von ihnen für die Besorgung ihrer Aufgaben verwendeten Daten Auftraggeber im Sinne des § 4 Z 4 des Datenschutzgesetzes 2000, BGBl. I Nr. 165/1999. Sie dürfen die ihnen zur Kenntnis gelangten Daten über ihre Kunden – soweit keine besonderen vertraglichen Vereinbarungen mit diesen bestehen – ausschließlich für den Zweck der Zustellung verwenden. Der Abschluss eines Vertrags über die Zustellleistung sowie der Inhalt eines solchen Vertrags dürfen nicht von der Zustimmung zur Weitergabe von Daten an Dritte abhängig gemacht werden; eine Weitergabe von Daten über Herkunft und Inhalt zuzustellender Dokumente an Dritte darf nicht vereinbart werden.

(5) Auf natürliche Personen, die an der Erbringung der Leistungen gemäß Abs. 1 und 2 mitwirken, ist in Hinblick auf Daten über Herkunft und Inhalt zuzustellender behördlicher Dokumente § 46 Abs. 1 bis 4 des Beamten-Dienstrechtsgesetzes 1979, BGBl. Nr. 333/1979, sinngemäß anzuwenden. Hinsichtlich der abgabenrechtlichen Geheimhaltungspflicht des § 48a der Bundesabgabenordnung, BGBl. Nr. 194/1961, gelten diese Personen als Beamte im Sinne des § 74 Abs. 1 Z 4 des Strafgesetzbuches, BGBl. Nr. 60/1974.

(6) Zustelldienste können in ihren allgemeinen Geschäftsbedingungen vorsehen, dass sie Zustellungen nur an bestimmte Personengruppen anbieten; Angehörige der betreffenden Personengruppe dürfen vom Abschluss eines Vertrags über die Zustellleistung gemäß Abs. 1 nicht ausgeschlossen werden. Einschränkungen in Hinblick auf die Herkunft der zuzustellenden behördlichen Dokumente dürfen nicht vorgesehen werden.

(7) Die Zustellleistung (Abs. 1) ist so zu erbringen, dass für behinderte Menschen ein barrierefreier Zugang zu dieser Leistung nach dem jeweiligen Stand der Technik gewährleistet ist.
<a name="p-30"></a>

### Zulassung als Zustelldienst

§ 30. (1) Die Erbringung der Zustellleistung ([§ 29 Abs. 1](#p-29)) bedarf einer Zulassung, deren Erteilung beim Bundeskanzler zu beantragen ist. Voraussetzungen für die Erteilung der Zulassung sind die für die ordnungsgemäße Erbringung der Zustellleistung erforderliche technische und organisatorische Leistungsfähigkeit sowie die rechtliche, insbesondere datenschutzrechtliche Verlässlichkeit des Zustelldienstes. Mit dem Antrag auf Zulassung sind allgemeine Geschäftsbedingungen vorzulegen, die den gesetzlichen Anforderungen zu entsprechen haben und der ordnungsgemäßen Erbringung der Zustellleistung nicht entgegenstehen dürfen.

(2) Der Zulassungsbescheid ist schriftlich zu erlassen; wenn es für die Gewährleistung der Leistungsfähigkeit und Verlässlichkeit erforderlich ist, sind darin Auflagen zu erteilen und Bedingungen vorzuschreiben.

(3) Der Bundeskanzler hat eine Liste der zugelassenen Zustelldienste einschließlich der in den Zulassungsbescheiden erteilten Auflagen und vorgeschriebenen Bedingungen (Abs. 2) und der gemäß [§ 31 Abs. 2 zweiter Satz](#p-31) erteilten Auflagen im Internet zu veröffentlichen.

(4) Wenn eine Zulassungsvoraussetzung wegfällt oder ihr ursprünglicher Mangel nachträglich hervorkommt, hat der Bundeskanzler die Behebung des Mangels innerhalb einer angemessenen Frist anzuordnen. Ist die Behebung des Mangels nicht möglich oder erfolgt sie nicht innerhalb der gesetzten Frist, ist die Zulassung durch Bescheid zu widerrufen.
<a name="p-31"></a>

### Aufsicht

§ 31. (1) Die Zustelldienste unterliegen der Aufsicht durch den Bundeskanzler. Sie sind verpflichtet, dem Bundeskanzler jede Änderung der die Voraussetzung der Zulassung gemäß [§ 30](#p-30) bildenden Umstände unverzüglich bekanntzugeben.

(2) Der Bundeskanzler hat die Aufsicht über die Zustelldienste dahin auszuüben, dass diese die Gesetze und Verordnungen nicht verletzen, insbesondere ihren Aufgabenbereich nicht überschreiten und die ihnen gesetzlich obliegenden Aufgaben erfüllen. Zu diesem Zweck ist der Bundeskanzler berechtigt, Auskünfte einzuholen und gegebenenfalls Auflagen vorzuschreiben, wenn die ordnungsgemäße Erbringung der Leistungen sonst nicht gewährleistet ist. Die Zustelldienste haben dem Bundeskanzler die geforderten Auskünfte unverzüglich, spätestens jedoch binnen zwei Wochen zu erteilen.
<a name="p-32"></a>

### Bestimmung des Ermittlungs- und Zustelldienstes

§ 32. (1) Zur Bestimmung des Ermittlungs- und Zustelldienstes hat der Bundeskanzler die Leistungen gemäß [§ 29 Abs. 1 Z 1 bis 9 und Abs. 2](#p-29) in einem gemeinsamen Vergabeverfahren im Sinne des Bundesvergabegesetzes 2006, BGBl. I Nr. 17/2006, auszuschreiben. Der Zuschlag darf nur einem zugelassenen Zustelldienst erteilt werden. Der Bundeskanzler hat den Zuschlagsempfänger und die Höhe des diesem für die Erbringung der Leistungen gemäß [§ 29 Abs. 1 Z 1 bis 9](#p-29) zustehenden Entgelts im Internet zu veröffentlichen.

(2) In Zeiträumen, in denen die Leistungen gemäß [§ 29 Abs. 2](#p-29) nicht von einem Ermittlungs- und Zustelldienst erbracht werden, sind sie durch einen beim Bundeskanzleramt eingerichteten Übergangszustelldienst zu erbringen. Der Übergangszustelldienst kann auch Leistungen gemäß [§ 29 Abs. 1](#p-29) erbringen und nachweisliche Zusendungen im Auftrag von Privaten gemäß [§ 29 Abs. 3](#p-29) vornehmen; er unterliegt nicht der Aufsicht gemäß [§ 31](#p-31). Die Leistungen gemäß [§ 29 Abs. 1 Z 1 bis 9 und Abs. 2](#p-29) sind unentgeltlich zu erbringen.
<a name="p-33"></a>

### An- und Abmeldung

§ 33. (1) Die Anmeldung bei einem Zustelldienst kann nur unter Verwendung der Bürgerkarte (§ 2 Z 10 E-GovG) erfolgen. Sofern es sich beim Kunden nicht um eine natürliche Person handelt, kann an die Stelle der Anmeldung mit der Bürgerkarte auch die Übermittlung der Daten aus dem elektronischen Rechtsverkehr (§§ 89a ff GOG) treten, die zu seinem Anschriftcode gespeichert und zum Nachweis der eindeutigen Identität geeignet sind. Jeder Zustelldienst hat im Internet ein elektronisches Verfahren für die Anmeldung bereitzustellen. Bei der Anmeldung sind folgende Daten zu speichern:


	1.  Name bzw. Bezeichnung des Kunden,
	2.  bei natürlichen Personen das Geburtsdatum,
	3.  die zur eindeutigen Identifikation des Kunden im Bereich „Zustellwesen“ erforderlichen Daten:
		a) bei natürlichen Personen das bereichsspezifische Personenkennzeichen (§ 9 E-GovG),
		b) sonst die Stammzahl (§ 6 E-GovG),
	4.  eine elektronische Adresse, an die die Verständigungen gemäß [§ 35 Abs. 1 und 2 erster Satz](#p-35) übermittelt werden können,
	5.  gegebenenfalls eine inländische Abgabestelle, an die die Verständigungen gemäß [§ 35 Abs. 2](#p-35) übermittelt werden können,
	6.  Angaben des Kunden darüber, welche Formate die zuzustellenden Dokumente aufweisen müssen, damit er zu ihrer Annahme bereit ist, und
	7.  Angaben des Kunden, die für eine allfällige inhaltliche Verschlüsselung der zuzustellenden Dokumente erforderlich sind.

Wurde als weitere Leistung im Sinne des [§ 29 Abs. 3](#p-29) vereinbart, dass die Verständigungen gemäß [§ 35](#p-35) an mehrere elektronische Adressen oder mehrere Abgabestellen zu übermitteln sind, sind alle Adressen zu speichern.

(2) Der Kunde hat Änderungen der in Abs. 1 genannten Daten dem Zustelldienst unverzüglich bekanntzugeben. Darüber hinaus kann er dem Zustelldienst mitteilen, dass die Zustellung innerhalb bestimmter Zeiträume ausgeschlossen sein soll.

(3) Die Abmeldung von einem Zustelldienst kann unter Verwendung der Bürgerkarte (§ 2 Z 10 E-GovG) oder durch eine vom Kunden unterschriebene schriftliche Erklärung erfolgen. Sie wird mit ihrem Einlangen beim Zustelldienst wirksam.
<a name="p-34"></a>

### Ermittlung des Zustelldienstes und Übermittlung des zuzustellenden Dokuments an diesen

§ 34. (1) Soll die Zustellung durch einen Zustelldienst erfolgen, so hat die Behörde den Ermittlungs- und Zustelldienst zu beauftragen, zu ermitteln, ob der Empfänger

	1.  bei einem Zustelldienst angemeldet ist und
	2.  die Zustellung nicht gemäß [§ 33 Abs. 2 zweiter Satz](#p-33) ausgeschlossen hat.

Liegen diese Voraussetzungen vor, so sind die Informationen gemäß [§ 33 Abs. 1 Z 6 und 7](#p-33) sowie die Internetadresse des Zustelldienstes, bei dem der Empfänger angemeldet ist, der Behörde zu übermitteln; andernfalls ist der Behörde mitzuteilen, dass diese Voraussetzungen nicht vorliegen. Steht der Behörde ein vom Empfänger akzeptiertes Format zur Verfügung, so hat sie das zuzustellende Dokument in diesem Format sowie gegebenenfalls in verschlüsselter Form dem Zustelldienst zu übermitteln.

(2) Eine Abfrage zur Ermittlung der in Abs. 1 angeführten Daten darf nur auf Grund eines Auftrags einer Behörde nach Abs. 1 oder zum Zweck der nachweislichen Zusendung von Dokumenten im Auftrag von Privaten ([§ 29 Abs. 3](#p-29)) vorgenommen werden. Als Suchkriterien dürfen nur die Daten gemäß [§ 33 Abs. 1 Z 1 bis 5](#p-33) verwendet werden.

(3) Bei der Auswahl zwischen mehreren in Betracht kommenden Zustelldiensten ist jenen der Vorzug zu geben, gegenüber denen der Empfänger Angaben über die inhaltliche Verschlüsselung ([§ 33 Abs. 1 Z 7](#p-33)) gemacht hat.
<a name="p-35"></a>

### Zustellung mit Zustellnachweis durch einen Zustelldienst

§ 35. (1) Der Zustelldienst hat den Empfänger unverzüglich davon zu verständigen, dass ein Dokument für ihn zur Abholung bereitliegt. Diese elektronische Verständigung ist an die dem Zustelldienst bekanntgegebene elektronische Adresse des Empfängers zu versenden. Hat der Empfänger dem Zustelldienst mehrere solcher Adressen bekanntgegeben, so ist die elektronische Verständigung an alle Adressen zu versenden; für die Berechnung der Frist gemäß Abs. 2 erster Satz ist der Zeitpunkt der frühesten Versendung maßgeblich. Die elektronische Verständigung hat jedenfalls folgende Angaben zu enthalten:


	1.  das Datum der Versendung,
	2.  die Internetadresse, unter der das zuzustellende Dokument zur Abholung bereitliegt,
	3.  das Ende der Abholfrist,
	4.  einen Hinweis auf das Erfordernis einer Signierung bei der Abholung von Dokumenten, die mit Zustellnachweis zugestellt werden sollen, und
	5.  einen Hinweis auf den Zeitpunkt, mit dem die Zustellung wirksam wird.

Soweit dies erforderlich ist, hat die Bundesregierung durch Verordnung nähere Bestimmungen über die Verständigungsformulare zu erlassen.

(2) Wird das Dokument nicht innerhalb von 48 Stunden abgeholt, so hat eine zweite elektronische Verständigung zu erfolgen; Abs. 1 dritter Satz ist sinngemäß anzuwenden.

(3) Der Zustelldienst hat sicherzustellen, dass zur Abholung bereitgehaltene Dokumente nur von Personen abgeholt werden können, die zur Abholung berechtigt sind und im Falle einer Zustellung mit Zustellnachweis oder einer nachweislichen Zusendung ihre Identität und die Authentizität der Kommunikation mit der Bürgerkarte (§ 2 Z 10 E-GovG) nachgewiesen haben. Zur Abholung berechtigt sind der Empfänger und, soweit dies von der Behörde nicht ausgeschlossen worden ist, eine zur Empfangnahme bevollmächtigte Person. Identifikation und Authentifizierung können auf Grund einer besonderen Vereinbarung des Empfängers mit dem Zustelldienst auch durch eine an die Verwendung sicherer Technik gebundene automatisiert ausgelöste Signatur erfolgen. Der Zustelldienst hat alle Daten über die Verständigungen gemäß Abs. 1 und 2 und die Abholung des Dokuments zu protokollieren und dem Absender unverzüglich zu übermitteln; die Gesamtheit dieser Daten bildet den Zustellnachweis.

(4) Der Zustelldienst hat das Dokument zwei Wochen zur Abholung bereitzuhalten. Wird das Dokument innerhalb dieser Frist nicht abgeholt, ist es zu löschen; andernfalls ist es nach Ablauf der Abholfrist (Abs. 1 Z 3) zwei weitere Wochen bereitzuhalten und danach, wenn zwischen Empfänger und Zustelldienst nicht anderes vereinbart wurde, zu löschen.

(5) Ein zur Abholung bereitgehaltenes Dokument gilt spätestens mit seiner Abholung als zugestellt.

(6) Die Zustellung gilt als am ersten Werktag nach der Versendung der ersten elektronischen Verständigung bewirkt, wobei Samstage nicht als Werktage gelten. Sie gilt als nicht bewirkt, wenn sich ergibt, dass die elektronischen Verständigungen nicht beim Empfänger eingelangt waren, doch wird sie mit dem dem Einlangen einer elektronischen Verständigung folgenden Tag innerhalb der Abholfrist (Abs. 1 Z 3) wirksam.

(7) Die Zustellung gilt als nicht bewirkt, wenn sich ergibt, dass der Empfänger


	1.  von den elektronischen Verständigungen keine Kenntnis hatte oder 
	2.  von diesen zwar Kenntnis hatte, aber während der Abholfrist von allen Abgabestellen ([§ 2 Z 4](#p-2)) nicht bloß vorübergehend abwesend war, doch wird die Zustellung an dem der Rückkehr an eine der Abgabestellen folgenden Tag innerhalb der Abholfrist wirksam, an dem das Dokument abgeholt werden könnte 

(8) Wurde dieselbe elektronische Verständigung an mehrere elektronische Adressen versendet, so ist der Zeitpunkt der frühesten Versendung maßgeblich.

(9) Leitet der Zustelldienst ein zuzustellendes Dokument zur elektronischen Übermittlung nach den §§ 89a ff GOG weiter, ist die Zustellung nach diesen Bestimmungen vorzunehmen.
<a name="p-36"></a>

### Zustellung ohne Zustellnachweis durch einen Zustelldienst

§ 36. Für die Zustellung ohne Zustellnachweis durch einen Zustelldienst gilt [§ 35](#p-35) mit der Maßgabe, dass die gemäß Abs. 3 letzter Satz übermittelten Daten nicht als Zustellnachweis gelten.
<a name="p-37"></a>

### Zustellung an einer elektronischen Zustelladresse oder über das elektronische Kommunikationssystem der Behörde

§ 37. (1) Zustellungen ohne Zustellnachweis können auch an einer elektronischen Zustelladresse oder über das elektronische Kommunikationssystem der Behörde erfolgen. Das Dokument gilt mit dem Zeitpunkt des Einlangens bzw. nach dem erstmaligen Bereithalten des Dokuments beim bzw. für den Empfänger als zugestellt. Bestehen Zweifel darüber, ob bzw. wann das Dokument beim Empfänger eingelangt ist bzw. für ihn bereitgehalten wird, hat die Behörde Tatsache und Zeitpunkt des Einlangens bzw. der Bereithaltung von Amts wegen festzustellen.

(1a) Das elektronische Kommunikationssystem der Behörde hat den Empfänger unverzüglich davon zu verständigen, dass ein Dokument für ihn zur Abholung bereitliegt. Diese elektronische Verständigung ist an die dem Kommunikationssystem der Behörde bekanntgegebene elektronische Adresse des Empfängers zu versenden. Hat der Empfänger mehrere solcher Adressen bekanntgegeben, so ist die elektronische Verständigung an alle Adressen zu versenden.

(2) Bevor eine Zustellung über das elektronische Kommunikationssystem erfolgt, hat die Behörde einen Auftrag gemäß [§ 34 Abs. 1](#p-34) zu erteilen. Die Zustellung über das elektronische Kommunikationssystem ist unzulässig, wenn sich ergibt, dass die Voraussetzungen für die Zustellung durch einen Zustelldienst vorliegen.

(3) Das elektronische Kommunikationssystem der Behörde hat die Weiterleitung der das Dokument beschreibenden Daten sowie die elektronische Information für die technische Möglichkeit der elektronischen identifizierten und authentifizierten Abholung des Dokuments dem Anzeigemodul ([§ 37b](#p-37b)) anzubieten.
<a name="p-37a"></a>

### Unmittelbare elektronische Ausfolgung

§ 37a. Versandbereite Dokumente können dem Empfänger unmittelbar elektronisch ausgefolgt werden, wenn dieser bei der Antragstellung seine Identität und die Authentizität der Kommunikation nachgewiesen hat und die Ausfolgung in einem so engen zeitlichen Zusammenhang mit der Antragstellung steht, dass sie von diesem Nachweis umfasst ist. Wenn mit Zustellnachweis zuzustellen ist, sind die Identität und die Authentizität der Kommunikation mit der Bürgerkarte (§ 2 Z 10 E-GovG) nachzuweisen.
<a name="p-37b"></a>

### Anzeigemodul

§ 37b. (1) Das Anzeigemodul ermöglicht Empfängern online die Anzeige der das Dokument beschreibenden Daten von zur Abholung für sie bereitgehaltenen Dokumenten sowie die Abholung dieser Dokumente.

(2) Der Betreiber des Anzeigemoduls ist gesetzlicher Dienstleister gemäß § 10 Abs. 2 des Datenschutzgesetzes 2000, BGBl. I Nr. 165/1999, für elektronische Zustelldienste, elektronische Kommunikationssysteme der Behörden, den Elektronischen Rechtsverkehr gemäß § 89a GOG und FinanzOnline zum Zweck der Identifikation und Authentifikation von zur Abholung berechtigten Personen.

(3) Das Anzeigemodul hat sämtliche Daten über die Abholung durch den Empfänger zu protokollieren und an das jeweilige Zustellsystem gemäß Abs. 2 elektronisch zu übermitteln.

(4) Die Bundesministerin oder der Bundesminister für Finanzen stellt ein Anzeigemodul zur Verfügung. Dieses kann auf Internetportalen von Behörden unter der Maßgabe der Einhaltung der technischen Schnittstellen und Spezifikationen angebunden werden. Die Bundesministerin oder der Bundesminister für Finanzen hat diese Schnittstellen und Spezifikationen im Internet auf ihrer oder seiner Website bekannt zu geben. Das Unternehmensserviceportal und das Bürgerserviceportal gemäß § 3 des Unternehmensserviceportalgesetzes, BGBl. I Nr. 52/2009, haben das Anzeigemodul für Unternehmen bzw. Bürgerinnen und Bürger einzubinden.

(5) Die Leistungen des Anzeigemoduls (Abs. 1) sind so zu erbringen, dass für Menschen mit Behinderung ein barrierefreier Zugang zu dieser Leistung nach dem jeweiligen Stand der Technik gewährleistet ist.

(6) Soweit dies erforderlich ist, hat die Bundesregierung durch Verordnung nähere Bestimmungen über die beschreibenden Daten von Dokumenten gemäß Abs. 1 zu erlassen.

(7) Die Bundesministerin oder der Bundesminister für Finanzen hat den einliefernden Systemen die Kosten für das Anzeigemodul entsprechend ihrem Einlieferungsvolumen zu verrechnen. Abweichend davon kann die Bundesministerin oder der Bundesminister für Finanzen in einer Verordnung auch die Verrechnung von Pauschalbeträgen festsetzen. Der IT-Dienstleister des Bundes, die Bundesrechenzentrum GmbH, kann als Zahlstelle eingerichtet werden.

(8) Die Verfügbarkeit des Anzeigemoduls ist von der Bundesministerin oder dem Bundesminister für Finanzen im Bundesgesetzblatt kundzumachen.
<a name="p-38"></a>

## 4. Abschnitt: Schlußbestimmungen

### Verweisungen

§ 38. (1) Verweisungen in den Verfahrensvorschriften auf Bestimmungen, die Angelegenheiten des Zustellwesens regeln, gelten als Verweisungen auf die entsprechenden Bestimmungen dieses Bundesgesetzes.

(2) Soweit in diesem Bundesgesetz auf Bestimmungen anderer Bundesgesetze verwiesen wird, sind diese in ihrer jeweils geltenden Fassung anzuwenden.
<a name="p-39"></a>

### Vollziehung

§ 39. Mit der Vollziehung dieses Bundesgesetzes ist hinsichtlich der [§§ 30](#p-30) bis [32](#p-32) der Bundeskanzler, hinsichtlich [§ 37b Abs. 1 bis 5, 7 und 8](#p-37b) die Bundesministerin oder der Bundesminister für Finanzen, hinsichtlich der übrigen Bestimmungen die Bundesregierung betraut.
<a name="p-40"></a>

### Inkrafttreten

§ 40. (1) [§ 15 Abs. 1](#p-15) in der Fassung des Bundesgesetzes BGBl. I Nr. 158/1998 tritt mit 1. Jänner 1998 in Kraft. Die §§ 1 Abs. 2, 2a samt Überschrift, 7 samt Überschrift, die Überschrift vor § 8a, die §§ 8a, 9, 10, 24 samt Überschrift, 26 Abs. 2 und 26a in der Fassung des Bundesgesetzes BGBl. I Nr. 158/1998 treten mit 1. Jänner 1999 in Kraft. § 1 Abs. 3, § 1a und die Überschrift zu § 10 treten mit Ablauf des 31. Dezember 1998 außer Kraft.

(2) § 1 Abs. 2 letzter Satz, § 2a Abs. 2, § 11 Abs. 3 und § 12 Abs. 4 in der Fassung des Bundesgesetzes BGBl. I Nr. 137/2001 treten mit 1. Jänner 2002 in Kraft.

(3) § 1 Abs. 2 letzter Satz und § 17a samt Überschrift in der Fassung des Verwaltungsreformgesetzes 2001, BGBl. I Nr. 65/2002, treten mit 1. Jänner 2002, jedoch nicht vor dem der Kundmachung des genannten Gesetzes folgenden Tag, in Kraft.

(4) Der Titel, §§ 1 bis 7 und 9 samt Überschriften, die Überschrift des Abschnitts II und die §§ 26 und 27 samt Überschriften, Abschnitt III, die Bezeichnungen des nunmehrigen Abschnitt IV und der nunmehrigen §§ 38, 39 und 40 sowie § 40 Abs. 4 und 5 (Anm.: in der Aufzählung fehlt Abs. 6) in der Fassung des Bundesgesetzes BGBl. I Nr. 10/2004 treten mit 1. März 2004 in Kraft. Zugleich treten § 8a, § 13 Abs. 5 und 6, § 17a und § 26a, in der zu diesem Zeitpunkt geltenden Fassung, außer Kraft.

(5) Die Bezeichnung des 1. Abschnitts, § 2 Z 2, Z 4, 5, 6 und 8 (Z 3 bis 6 neu) und Z 7 bis 9, die §§ 3 bis 5 samt Überschriften, § 7, § 9 Abs. 1 bis 3 und 6, § 10 samt Überschrift, § 12 samt Überschrift, die Bezeichnung und die Überschrift des 2. Abschnitts, § 13, § 14, § 16 Abs. 1, 3 und 4, § 17, § 18, § 19, § 20 Abs. 1 und 2, § 21 samt Überschrift, § 22 Abs. 2 bis 4, § 23 Abs. 1, 2 und 4, § 24 samt Überschrift, § 24a samt Überschrift, § 25 Abs. 1, § 26 Abs. 1, § 27 samt Überschrift, der 3. Abschnitt, die Bezeichnung des 4. Abschnitts, § 39, § 40 Abs. 5 und § 41 samt Überschrift in der Fassung des Bundesgesetzes BGBl. I Nr. 5/2008 treten mit 1. Jänner 2008 in Kraft; gleichzeitig treten § 2 Z 3 und 7, die Überschriften nach § 8 (zum früheren § 8a) und nach § 17 (zum früheren § 17a) außer Kraft. § 37 samt Überschrift in der Fassung des Art. 4 Z 48 des Bundesgesetzes BGBl. I Nr. 5/2008 tritt mit 1. Jänner 2009 in Kraft. Die Zustelldiensteverordnung – ZustDV, BGBl. II Nr. 233/2005, gilt in ihrer am 31. Dezember 2007 geltenden Fassung weiter.

(6) Das Vergabeverfahren gemäß § 32 Abs. 1 ist spätestens neun Monate, nachdem zumindest drei elektronische Zustelldienste zugelassen worden sind, einzuleiten. Bis zur Erteilung des Zuschlags nach § 32 Abs. 1 beträgt das den zugelassenen elektronischen Zustelldiensten zu entrichtende Entgelt für die Leistungen gemäß § 29 Abs. 1 Z 1 bis 9 die Hälfte des in den allgemeinen Geschäftsbedingungen für den reservierten Postdienst (§ 9 Abs. 1 des Postgesetzes 1997) vorgesehenen Standardtarifs für Briefsendungen. Zusätzlich können die aufgrund § 37b Abs. 7 anfallenden Kosten mit dem zu entrichtenden Entgelt weiterverrechnet werden.

(7) § 22 Abs. 3, § 27 Z 3, § 28 Abs. 2, § 29 Abs. 1 Z 10 und 11, § 33 Abs. 1 und § 35 Abs. 9 in der Fassung des Budgetbegleitgesetzes 2011, BGBl. I Nr. 111/2010, treten mit 1. Jänner 2011 in Kraft.

(8) § 2 Z 1, 6 und 7, § 10 samt Überschrift, § 11 Abs. 2, § 18 Abs. 1 Z 1, § 19 samt Überschrift, § 22 Abs. 2 und 4, § 25 Abs. 1, § 27 Z 2, § 29 Abs. 1 Z 7, 8 und 11 und § 35 Abs. 3 letzter Satz in der Fassung des Bundesgesetzes BGBl. I Nr. 33/2013 treten mit Ablauf des Monats der Kundmachung dieses Bundesgesetzes in Kraft.

(9) In der Fassung des Deregulierungsgesetzes 2017, BGBl. I Nr. 40/2017, treten in Kraft:


	1.  § 11 Abs. 2 mit 1. März 2014, 
	2.  § 2 Z 7 bis 9, die Überschrift zu § 10, § 28 Abs. 2, § 29 Abs. 5, § 32 Abs. 1, § 35 Abs. 1 Z 4, Abs. 2, 3 erster Satz, 6 bis 8, § 36, § 37 Abs. 1 und 1a, § 37b samt Überschrift sowie § 39 mit Ablauf des Tages der Kundmachung und 
	3.  § 29 Abs. 1 Z 11 und 12, § 37 Abs. 3 sowie § 40 Abs. 6 zweiter Satz mit Beginn des siebenten auf den Tag der Kundmachung der Verfügbarkeit des Anzeigemoduls gemäß § 37b Abs. 8 folgenden Monats.(Anm. 1) 
---
(Anm. 1: Die Kundmachung erfolgte am 30.5.2018 mit BGBl. I Nr. 33/2018.)
<a name="p-41"></a>

### Sprachliche Gleichbehandlung

§ 41. Soweit sich die in diesem Bundesgesetz verwendeten Bezeichnungen auf natürliche Personen beziehen, gilt die gewählte Form für beide Geschlechter. Bei der Anwendung dieser Bezeichnungen auf bestimmte natürliche Personen ist die jeweils geschlechtsspezifische Form zu verwenden.

### Artikel 5

Notifikationshinweis gemäß Artikel 12 der Richtlinie 83/189/EWG

(Anm.: aus BGBl. I Nr. 5/2008, zu den §§ 2 - 5, 7, 9, 10, 12 - 14, 16 - 37a und 41, BGBl. Nr. 200/1982)

Dieses Bundesgesetz wurde unter Einhaltung der Bestimmungen der Richtlinie 83/189/EWG des Rates über ein Informationsverfahren auf dem Gebiet der Normen und technischen Vorschriften in der Fassung der Richtlinien 88/182/EWG und 94/10/EWG der Europäischen Kommission notifiziert (Notifikationsnummer 2007/549/A).

